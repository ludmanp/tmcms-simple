Generate API docs:
apigen generate --source src --destination api --no-source-code

###Copy Entity

In Entity Class

```use copyableEntityTrait;```

For submodel copying

```
public function copy(){
    oldid = $this->getId();
    parent::copy();
    $childs = ChildEntityRepository::getInstance()
        ->setWhereParentId($oldid)
        ->getAsArrayOfObjects();
    foreach($childs as $child){
        $child->copy();
}
```

In CmsController create column

```->addColumn($this->copyColumn())```

There are paramerets and options in `copyColumn` to customize column
 

