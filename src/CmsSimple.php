<?php

namespace TMCms\Modules\Simple;


use TMCms\Admin\Messages;
use TMCms\DB\SQL;
use TMCms\HTML\BreadCrumbs;
use TMCms\HTML\Cms\Column\ColumnData;
use TMCms\Log\App;
use TMCms\Modules\Gallery\ModuleGallery;
use TMCms\Modules\Images\Entity\ImageEntityRepository;
use TMCms\Orm\Entity;
use TMCms\Orm\EntityRepository;
use TMCms\Strings\Converter;

/**
 * Class CmsSimple
 * @package TMCms\Modules\Simple
 */
class CmsSimple
{
    protected $_default_entity = null;

    /**
     * @param null $model
     * @return BreadCrumbs
     */
    protected function getBreadCrumbs($model = false)
    {
        if($model===false)
            $model = empty($_GET['m']) ? '' : $_GET['m'];

        if($model){
            $url = '?p=' . P . '&do='.$model;
            if(isset($_GET['parent_id'])){
                $url .= '&parent_id='.$_GET['parent_id'];
            }
            return BreadCrumbs::getInstance()
                ->addCrumb(__(ucfirst(Converter::pluralize(str_replace('_', ' ', $model)))), $url);
        }else{
            return BreadCrumbs::getInstance();
        }
    }

    /**
     *
     */
    protected function goToList()
    {
        $get = $_GET;
        if(empty($_GET['m']))
            unset($get['do']);
        else
            $get['do'] = $_GET['m'];

        go('?'.http_build_query($get)); // Go to main page
    }

    /**
     * @param int $id
     * @param null $model
     * @return Entity
     */
    protected function getEntity($id = 0, $model = null){
        if(!$model)
            $model = empty($_GET['m']) ? '' : $_GET['m'];
        if($model){
            $entity = 'TMCms\Modules\\'.ucfirst(P).'\Entity\\' . Converter::toCamelCase($model) . 'Entity';
            return new $entity($id);
        }else {
            if (!empty($this->_default_entity)){
                if (is_string($this->_default_entity))
                    return new $this->_default_entity($id);
                else
                    $class = $this->_default_entity->get_class();
                    return $class::getInstance($id);
            }else{
                return null;
            }
        }
    }

    /**
     *
     */
    public function add()
    {
        $model = empty($_GET['m']) ? '' : $_GET['m'];
        $this->getBreadCrumbs()->addCrumb(__('Add'));
        $this->beforeAdd($model);
        $function = ($model ? '__' . $model : '_') . '_add_edit_form';
        echo $this->$function();
    }

    /**
     *
     */
    public function beforeEdit($model, $data)
    {

    }

    /**
     *
     */
    public function beforeAdd($model)
    {

    }

    public function edit()
    {
        $id = abs((int)$_GET['id']);
        if (!$id) return;
        $data = $this->getEntity($id);

        $title = $data->getFormTitle() ?: ($data->getTitle() ?: $data->getName());
        $this->getBreadCrumbs()->addCrumb($title);
        $model = empty($_GET['m']) ? '' : $_GET['m'];

        $this->beforeEdit($model, $data);

        $function = ($model ? '__' . $model : '_') . '_add_edit_form';
        echo $this->$function($data);
    }

    public function _add()
    {
        // Create empty object
        $object = $this->getEntity();
        // Set all supplied data
        $object->loadDataFromArray($_POST);
        // Persist all data in database
        $object->save();

        $model = empty($_GET['m']) ? '' : $_GET['m'];
        $title = $object->getTitle() ?: $object->getName();
        Messages::sendGreenAlert(Converter::charsToNormalTitle($model) . ' ' . $title . ' added');

        $this->goToList();
    }

    public function _edit()
    {
        $object = $this->getEntity($_GET['id']);
        // Set all supplied data
        $object->loadDataFromArray($_POST);
        // Persist all data in database
        $object->save();

        $model = empty($_GET['m']) ? '' : $_GET['m'];
        $title = $object->getTitle() ?: $object->getName();
        Messages::sendGreenAlert(Converter::charsToNormalTitle($model) . ' ' . $title . ' edited');

        if (IS_AJAX_REQUEST) {
            die('1');
        }
        $this->goToList();
    }

    public function _delete()
    {
        $object = $this->getEntity($_GET['id']);
        // Delete data from database
        $object->deleteObject();

        $model = empty($_GET['m']) ? '' : $_GET['m'];
        Messages::sendGreenAlert(Converter::charsToNormalTitle($model) . ' with id ' . $object->getId() . ' deleted');
        App::add(Converter::charsToNormalTitle($model) . ' with id ' . $object->getId() . ' deleted');

        back(); // Redirect to previous page
    }

    public function _active()
    {
        $id = abs((int)$_GET['id']);
        if (!$id) return;

        $object = $this->getEntity($_GET['id']);
        $object->flipBoolValue('active');
        $object->save();

        $model = empty($_GET['m']) ? '' : $_GET['m'];
        $title = $object->getTitle() ?: $object->getName();
        App::add(Converter::charsToNormalTitle($model) . ' "' . $title . '" ' . ($object->getActive() ? '' : 'de') . 'activated');
        Messages::sendGreenAlert(Converter::charsToNormalTitle($model) . ' ' . ($object->getActive() ? '' : 'de') . 'activated');

        if (IS_AJAX_REQUEST) {
            die('1');
        }
        back();
    }

    public function _order()
    {
        $model = empty($_GET['m']) ? '' : $_GET['m'];
        $entity = $this->getEntity($_GET['id']);

        SQL::order($_GET['id'], $entity->getDbTableName(), $_GET['direct'], 'id', 'order');
        $m_name = __(ucfirst(str_replace('_', ' ', $model)));
        App::add($m_name.' order changed');
        Messages::sendMessage($m_name.' order changed');

        back();
    }

    public function _copy()
    {
        $entity = $this->getEntity($_GET['id']);
        $entity->copy();
        go('?p='.P.'&do=edit&id='.$entity->getId().$this->mParam());
    }


    public function getFormAction($id = 0)
    {
        return '?p=' . P . '&do=_'.($id ? 'edit&id='. $id : 'add') . $this->mParam();
    }


    private function mParam($options = []){
        if(!empty($_GET['m'])){
            return '&m='.$_GET['m'];
        }elseif(!empty($options['m'])){
            return '&m='.$options['m'];
        }
        return '';
    }

    /** IMAGES */
    /**
     * @param EntityRepository $items
     * @param Entity|null $item
     * @return $this
     */
    public function addGalleryToList(EntityRepository $items, Entity $item = null){
        if(is_null($item))
            $item = $this->getEntity();
        $entity_class = strtolower(Converter::classWithNamespaceToUnqualifiedShort($item));
        $images = new ImageEntityRepository();
        return $items->addSimpleSelectFieldsAsString('(SELECT COUNT(*) FROM `'. $images->getDbTableName() .'` '
            .'WHERE `item_id` = `'. $items->getDbTableName() .'`.`id` '
            .'AND `item_type` = "'. $entity_class . '") AS `images`');

    }

    /**
     * @param bool|false $link
     * @param string $title
     * @return ColumnData
     */
    public function imageColumn($link = false, $title = "images", $options = []) : ColumnData
    {
        if(!$link) {
            $link = '?p=' . P . '&do=images&id={%id%}';
            $link .= $this->mParam($options);
            if(!empty($_GET['parent_id'])){
                $link .= '&parent_id='.$_GET['parent_id'];
            }
        }
        $value = $title;
        if(!empty($options['value']) && $options['value']){
            $value = $options['value'];
        }
        return ColumnData::getInstance($title)
            ->enableNarrowWidth()
            ->setTitle($title)
            ->setValue($value)
            ->setAlign('center')
            ->allowHtml()
            ->setHref($link);
    }

    protected function copyColumn($link = false, $title = 'Copy', $options = []) : ColumnData
    {
        if(!$link) {
            $link = '?p=' . P . '&do=_copy&id={%id%}';
            $link .= $this->mParam($options);
            if(!empty($_GET['parent_id'])){
                $link .= '&parent_id='.$_GET['parent_id'];
            }
        }
        if(!empty($options['value']) && $options['value']){
            $value = $options['value'];
        }elseif(!empty($options['icon']) && $options['icon']){
            $value = '<i class="fa fa-'.$options['icon'].'">';
        }else{
            $value = '<i class="fa fa-copy">';
        }
        return ColumnData::getInstance($title)
            ->setHref($link)
            ->allowHtml()
            ->setAlign( !empty($options['align']) ? $options['align'] : 'center' )
            ->setValue($value)
            ->disableNewlines()
            ->enableNarrowWidth();
    }

    public function images() {
        $id = abs((int)$_GET['id']);
        if (!$id) return;

        $this->getBreadCrumbs()->addCrumb(__('Images'));
        $entity = $this->getEntity($id);
        echo ModuleGallery::getViewForCmsModules($entity);
    }


    public function _images_delete() {
        $id = abs((int)$_GET['id']);
        if (!$id) return;

        ModuleGallery::deleteImageForCmsModules($id);

        back();
    }

    public function _images_order() {
        $id = abs((int)$_GET['id']);
        if (!$id) return;

        ModuleGallery::orderImageForCmsModules($id, $_GET['direct']);

        back();
    }

    public function _images_active()
    {
        ModuleGallery::activeImageForCmsModules($_GET['id']);
    }
    
}