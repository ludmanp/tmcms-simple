<?php

namespace TMCms\Modules\Simple\Entity;


use TMCms\Orm\Entity;
use TMCms\Strings\Converter;

/**
 * Class ActiveEntity
 * @package TMCms\Modules\Simple\Entity
 */
class ActiveEntity extends Entity
{

    protected $activeFieldName = 'active';
    protected function beforeSave()
    {
        if(!empty($_POST) && !isset($_POST[$this->activeFieldName])){
            $this->{'set' . Converter::toCamelCase($this->activeFieldName)}(0);
        }
    }
}