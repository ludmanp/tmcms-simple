<?php

namespace TMCms\Modules\Simple;

use DrewM\MailChimp\MailChimp;
use PHPMailer;
use Tests\TMCms\Templates\PageHeadTest;
use TMCms\Config\Configuration;
use TMCms\Config\Settings;
use TMCms\Modules\IModule;
use TMCms\Modules\Products\Entity\ProductEntity;
use TMCms\Modules\Products\Entity\WinesOfWeekEntity;
use TMCms\Network\Mailer;
use TMCms\Orm\Entity;
use TMCms\Orm\EntityRepository;
use TMCms\Templates\PageHead;
use TMCms\Traits\singletonInstanceTrait;

/**
 * Class ModuleSimple
 * @package TMCms\Modules\Simple
 */
class ModuleSimple implements IModule {
    use singletonInstanceTrait;

    static function makeTitle($sequence, $delim=null){
        $cnf = \TMCms\Config\Configuration::getInstance()->get('seo');
        return self::makeSequence($sequence, $delim?$delim:$cnf['title_delim'], $cnf['title_max_length']);
    }

    static function makeDescription($sequence, $delim=null){
        $cnf = \TMCms\Config\Configuration::getInstance()->get('seo');
        return self::makeSequence($sequence, $delim?$delim:$cnf['description_delim'], $cnf['description_max_length']);
    }

    static public function makeSequence($sequence, $delimiter, $max_length)
    {
        if(!is_array($sequence))
            $sequence = array($sequence);
        $res = '';
        foreach ($sequence as $s){
            $not_cut = false;
            if(!$s)
                continue;
            if(is_array($s)){
                if(isset($s['value'])) {
                    if(!empty($s['not_cut'])){
                        $str = $s['value'];
                        $not_cut = true;
                    }else {
                        $str = self::makeSequence(explode(' ', str_replace(chr(10), ' ', $s['value'])), ' ', $max_length - strlen($res) - strlen($delimiter));
                    }
                }else {
                    continue;
                }
            }else {
                // add delimiter before element when result is not empty
                $str = $s;
            }
            $str = $res ? $delimiter . trim($str) : trim($str);
            // if length or new result is less then $cnf->title_max_length add element
            // otherwise return result
            if($not_cut || (strlen($res) < $max_length && strlen($res . $str) <= $max_length)){
                $res .= $str;
            }else{
                return $res;
            }
        }
        // if no more elements return as is;
        return str_replace('"', '', $res);
    }

    static public function DateSql2Arr($date = "") {
        $retv = array();
        if (!empty($date)) {
            $retv['day'] = substr($date,8,2);
            $retv['month'] = substr($date,5,2);
            $retv['year'] = substr($date,0,4);
            $retv['hour'] = substr($date,11,2);
            $retv['min'] = substr($date,14,2);
            $retv['sec'] = substr($date,17);
        }
        return $retv;
    }

    static public function DateSql2Html($date = "", $nosec = false) {
        $retv = "";
        if (!empty($date)) {
            if (strlen($date) > 10) {
                if ($nosec) {
                    $retv = substr($date,8,2).".".substr($date,5,2).".".substr($date,0,4).substr($date,10,6);
                    if (substr($retv,-5) == "00:00")
                        $retv = substr($retv,0,strlen($retv) - 5);
                }
                else
                    $retv = substr($date,8,2).".".substr($date,5,2).".".substr($date,0,4).substr($date,10);
            }
            else
                $retv = substr($date,8,2).".".substr($date,5,2).".".substr($date,0,4);
        }
        return $retv;
    }

    public static function dateToCal($date, $diff_hours=0){

        return date('Ymd\THis\Z',  strtotime($date) - $diff_hours * 60 * 60);
    }

    static $date_langs = [
        'ru'=>[
            'weekdays'=>['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота',],
            'months'=>['января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря',],
            'months_short'=>['янв','фев','мар','апр','мая','июн','июл','авг','сен','окт','ноя','дек',],
        ],
        'en'=>[
            'weekdays'=>['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday',],
            'months'=>['January','February','March','April','May','June','July','August','September','October','November','December',],
            'months_short'=>['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec',],
        ],
        'lv'=>[
            'weekdays'=>['svētdiena','pirmdiena','otrdiena','trešdiena','ceturtdiena','piektdiena','sestdiena',],
            'months'=>['janvāris','februāris','marts','aprīlis','maijs','jūnijs','jūlijs','augusts','septembris','oktobris','novembris','decembris',],
            'months_short'=>['jan','feb','mar','apr','mai','jūn','jūl','aug','sep','okt','nov','dec',],
        ],
    ];

    static public function shortMonth($month, $lang = LNG)
    {
        if($month>=1 && $month<=12) {
            $f = isset(self::$date_langs[$lang]) ? self::$date_langs[$lang] : self::$date_langs['en'];
            return $f['months_short'][$month-1];
        }else {
            return $month;
        }
    }

    static public function FullDate($date, $lang = LNG){
        $dt = self::DateSql2Arr($date);
        if($dt['day']==0)
            return '';

        $f = isset(self::$date_langs[$lang]) ? self::$date_langs[$lang] : self::$date_langs['en'];
        switch($lang){
            case 'ru':
                return $f['weekdays'][date( "w", strtotime($date))].', '.$dt['day']. ' '. $f['months'][$dt['month']-1].' '.$dt['year'].' г.';
            case 'lv':
                return $f['weekdays'][date( "w", strtotime($date))].', '.$dt['year'].'. gada '.$dt['day']. '. '. $f['months'][$dt['month']-1];
            default:
                return $f['weekdays'][date( "w", strtotime($date))].', '.$dt['day']. '. '. $f['months'][$dt['month']-1].', '.$dt['year'];
        }

    }

    static public function SqlNow()
    {
        return date('Y-m-d H:i');
    }


    static public function copyrightYear($year){
        $y = date('Y');
        return $year . ($y==$year ? '' : '-'.$y);
    }

    /**
     * Возвращает сумму прописью
     * @author runcore
     * @uses morph(...)
     * взято отсюда http://habrahabr.ru/post/53210/
     */
    static function num2str($num, $lang="ru") {
        switch($lang){
            case 'lv':
                $nul='nulle';
                $ten=array(
                    array('','viens','divi','trīs','četri','pieci','seši','septiņi', 'astoņi','deviņi'),
                    array('','viens','divi','trīs','četri','pieci','seši','septiņi', 'astoņi','deviņi'),
                );
                $a20=array('desmit','vienpadsmit','divpadsmit','trīspadsmit','četrpadsmit' ,'piecpadsmit','sešpadsmit','septiņpadsmit','astoņpadsmit','deviņpadsmit');
                $tens=array(2=>'divdesmit','trīsdesmit','četrdesmit','piecdesmit','sešdesmit','septiņdesmit' ,'astoņdesmit','deviņdesmit');
                $hundred=array('','simts','divi simti','trīs simti','četri simti','pieci simti','seši simti', 'septiņi simti','astoņi simti','deviņi simti');
                $unit=array( // Units
                    array('cents' ,'centi' ,'centi',	 1),
                    array('eiro'   ,'eiro'   ,'eiro'    ,0),
                    array('tukstotis'  ,'tukstoši'  ,'tukstoši'     ,1),
                    array('miljons' ,'miljoni','miljoni' ,0),
                    array('miljards','miljardi','miljardi',0),
                );
                break;
            case 'en':
                $nul='nil';
                $ten=array(
                    array('','one','two','three','four','five','six','seven', 'eight','nine'),
                    array('','one','two','three','four','five','six','seven', 'eight','nine'),
                );
                $a20=array('ten','eleven','twelve','thirteen','fourteen' ,'fifteen','sixteen','seventeen','eighteen','nineteen');
                $tens=array(2=>'twenty','thirty','forty','fifty','sixty','seventy' ,'eighty','ninety');
                $hundred=array('','hundred','two hundreds','three hundreds','four hundreds','five hundreds','six hundreds', 'seven hundreds','eight hundreds','nine hundreds');
                $unit=array( // Units
                    array('cent' ,'cents' ,'cents',	1),
                    array('euro'   ,'euro'   ,'euro', 0),
                    array('thousand'  ,'thousands', 'thousands' ,1),
                    array('million' ,'millions','millions' ,0),
                    array('milliard','milliards','milliards',0),
                );
                break;
            default:
                $nul='ноль';
                $ten=array(
                    array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
                    array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
                );
                $a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
                $tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
                $hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
                $unit=array( // Units
                    array('цент' ,'цента' ,'центов',	 1),
                    array('евро'   ,'евро'   ,'евро'    ,0),
                    array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
                    array('миллион' ,'миллиона','миллионов' ,0),
                    array('миллиард','милиарда','миллиардов',0),
                );
        }
        //
        list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub)>0) {
            foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
                if (!intval($v)) continue;
                $uk = sizeof($unit)-$uk-1; // unit key
                $gender = $unit[$uk][3];
                list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
                else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                // units without rub & kop
                if ($uk>1) $out[]= self::morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
            } //foreach
        }
        else $out[] = $nul;
        $out[] = self::morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
        $out[] = $kop.' '.self::morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
        return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
    }

    /**
     * Склоняем словоформу
     * @ author runcore
     */
    static function morph($n, $f1, $f2, $f5) {
        $n = abs(intval($n)) % 100;
        if ($n>10 && $n<20) return $f5;
        $n = $n % 10;
        if ($n>1 && $n<5) return $f2;
        if ($n==1) return $f1;
        return $f5;
    }


    static public function html_substr( $s, $srt, $len = NULL, $strict=false, $suffix = NULL )
    {
        if ( is_null($len) ){ $len = strlen( $s ); }

        // strips all tags but div, p and br and eliminates all attributes in tags
        $s = preg_replace('/<([a-z]+)[^>]*>/i', '<\1>', strip_tags($s, '<div><p><br><br/>'));

        $f = 'static $strlen=0;
			if ( $strlen >= ' . $len . ' ) { return "><"; }
			$html_str = html_entity_decode( $a[1] );
			$subsrt   = max(0, ('.$srt.'-$strlen));
			$sublen = ' . ( empty($strict)? '(' . $len . '-$strlen)' : 'max(@strpos( $html_str, "' . ($strict===2?'.':' ') . '", (' . $len . ' - $strlen + $subsrt - 1 )), ' . $len . ' - $strlen)' ) . ';
			$new_str = substr( $html_str, $subsrt,$sublen);
			$strlen += $new_str_len = strlen( $new_str );
			$suffix = ' . (!empty( $suffix ) ? '($new_str_len===$sublen?"'.$suffix.'":"")' : '""' ) . ';
			return ">" . htmlentities($new_str, ENT_QUOTES, "UTF-8") . "$suffix<";';

        $res = preg_replace( array( "#<[^/][^>]+>(?R)*</[^>]+>#", "#(<(b|h)r\s?/?>){2,}$#is"), "", trim( rtrim( ltrim( preg_replace_callback( "#>([^<]+)<#", create_function(
            '$a',
            $f
        ), ">$s<"  ), ">"), "<" ) ) );
        $res = preg_replace('~(<p></p>|<p>|<br>|<br/>)*$~i','', $res);
        return $res;
    }

    static function subscribe($formData, $lang){
        if(!Configuration::getInstance()->get('mailchimp')['api_key'] || !Configuration::getInstance()->get('mailchimp')['list_id'])
            return false;
        $mailchimp = new MailChimp(Configuration::getInstance()->get('mailchimp')['api_key']);
        $merge_fields = [ "mc_language" => $lang, ];
        if(!empty($formData['name'])){
            $merge_fields['FNAME'] = $formData['name'];
        }
        if(!empty($formData['surname'])){
            $merge_fields['LNAME'] = $formData['surname'];
        }
        $result = $mailchimp->post("lists/".Configuration::getInstance()->get('mailchimp')['list_id']."/members",
            array(
                'email_address'     => $formData['email'],
                'status'        => 'subscribed',
                'merge_vars'        => (object) $merge_fields,
                'update_existing'   => false,
                'send_welcome'      => false,
                'double_optin'      => false,
            )
        );
        $log = new \TMCms\Log\ErrorLogger();
        $log->log('Mailchimp result: '.print_r($result, true));
        return $result;
    }

    static function googleTrackingCode(array $options){
        if(empty($options['label']))
            throw new \Exception('Label should be defined for tracking code');
        $label = $options['label'];
        $config = Configuration::getInstance()->get('google');
        $conversion_id = !empty($options['conversion_id']) ? $options['conversion_id'] :  $config['conversion_id'];
        $title = !empty($options['title']) ? $options['title'] : '';
        $lang = !empty($options['lang']) ? strtoupper($options['lang']) : LNG;
        PageHead::getInstance()
        ->addJs("
            // Google Code for Noble Wine $title $lang Conversion Page
            /* <![CDATA[ */
            var google_conversion_id = $conversion_id;
            var google_conversion_language = \"$lang\";
            var google_conversion_format = \"3\";
            var google_conversion_color = \"ffffff\";
            var google_conversion_label = \"$label\";
        var google_remarketing_only = false;
        /* ]]> */
        ")
        ->addJsUrl("//www.googleadservices.com/pagead/conversion.js")
        ->addCustomString("
        <noscript>
            <div style=\"display:inline;\">
                <img height=\"1\" width=\"1\" style=\"border-style:none;\" alt=\"\" src=\"//www.googleadservices.com/pagead/conversion/$conversion_id/?label=$label&amp;guid=ON&amp;script=0\"/>
            </div>
        </noscript>
        ");
    }


    public static function getSitemapAjaxTab($tab_id, $ajax_link, $lng=LNG, $return_ids = false){
        if(substr($tab_id, 0, 1)!='#')
            $tab_id = '#'.$tab_id;
        $link = $ajax_link.'/'.$lng.'/';
        if($return_ids){
            $link .= '?return_ids=1';
        }
        return "
        <script>
            $(function(){
                $('#modal-popup_inner .nav-tabs a[href=$tab_id]').on('shown.bs.tab', function(e){
                    var tabpane = $('#modal-popup_inner .tab-pane$tab_id');
                    if(tabpane.data('loaded')) return;
                    tabpane.css({'min-height': 100}).spin();
                    $.get('$link', function(data){
                        tabpane.html(data).data('loaded', true);
                        if(typeof(tabpane.data('spinner')) != 'undefined'){
                            tabpane.data('spinner').stop();
                        }
                    })
                })
            })
        </script>
        ";
    }


    public static function getParam($name, $default = null)
    {
        if(isset($_POST[$name])){
            return $_POST[$name];
        }elseif(isset($_GET[$name])){
            return $_GET[$name];
        }else{
            return $default;
        }
    }


    public static function sendMail($subj, $body, $to, $from = null, $files = [])
    {
        if($smtp = Configuration::getInstance()->get('smtp')) {

            $mailer = new PHPMailer();

            //$mailer->SMTPDebug = 3;

            $mailer->isSMTP();                                      // Set mailer to use SMTP
            $mailer->CharSet = 'UTF-8';
            $mailer->Host = $smtp['host'];                          // Specify main and backup SMTP servers
            $mailer->SMTPAuth = true;                               // Enable SMTP authentication
            $mailer->Username = $smtp['username'];                  // SMTP username
            $mailer->Password = $smtp['password'];                  // SMTP password
            $mailer->SMTPAutoTLS = false;
            $mailer->SMTPSecure = '';                               // Enable TLS encryption, `ssl` also accepted
            //$mailer->Port = 587;                                  // TCP port to connect to
            $mailer->isHTML(true);

            $mailer->Subject = $subj;
            $mailer->Body    = $body;

            if(!$from){
                $from = [\TMCms\Config\Settings::getCommonEmail(), 'Noble Wine'];
            }
            if(is_array($from)) {
                $mailer->setFrom($from[0], $from[1]);
            }else {
                $mailer->setFrom($from);
            }
            foreach((array)$to as $e){
                if(is_array($e)){
                    $mailer->addAddress($e[0], $e[1]);     // Add a recipient
                }else{
                    $mailer->addAddress($e);     // Add a recipient
                }
            }
            if(!empty($files)) {
                foreach ((array)$files as $file) {
                    if($file) $mailer->addAttachment($file);         // Add attachments
                }
            }

            $mailer->send();
        }else{
            $mailer = Mailer::getInstance()->setSubject(w("New order received"))
                ->setMessage($body);
            if(is_array($from)) {
                $mailer->setSender($from[0], $from[1]);
            }else {
                $mailer->setSender($from);
            }
            foreach((array)$to as $e) {
                if(is_array($e)) {
                    $mailer->setRecipient($e[0], $e[1]);
                }else {
                    $mailer->setRecipient($e);
                }
            }
            if(!empty($files)) {
                foreach ((array)$files as $file) {
                    if($file) $mailer->addAttachment($file);         // Add attachments
                }
            }

            $mailer->send();
        }
    }

    static public function getMailBody($template, $model = [], $base='base')
    {
        // $template and $model is used inside base template
        ob_start();
        include DIR_BASE."email/$base.php";
        $body = ob_get_contents();
        ob_end_clean();
        return $body;
    }


    static public function text2Html($text){
        // is not html
        if($text == strip_tags($text)){
            return '<p>'.preg_replace('/(\r\n|\n|\r)/', '<br/>', $text).'</p>';
        }else{
            return $text;
        }
    }

    static public function phone2Link($phone, $attributes = []){
        $attrs = [];
        foreach($attributes as $name=>$val)
            $attrs[] = $name.'="'.$val.'"';
        return '<a href="tel:'.self::phone4Link($phone). '" '.implode(' ', $attrs).'>'.$phone.'</a>';
    }

    static public function phone4Link($phone){
        return preg_replace('~[^0-9+]+~', '', $phone);
    }
    static function imageAlt($image, $alt = ''){
        return \TMCms\Files\FileSystem::getImageFileAltProperty($image) ?: ($alt ?: '');
    }

    static public function getLangEntity($uid, EntityRepository $repo, $fields = [], $uid_name = 'uid'){
        $lngs = array_keys(\TMCms\Routing\Languages::getPairs());
        $trans = \TMCms\Admin\Structure\Entity\TranslationRepository::getInstance()->addSimpleSelectFields($lngs);
        $func = 'setWhere'.ucfirst($uid_name);
        $repo->{$func}($uid);
        $fields = array_merge(['id'], $fields);
        return $repo
            ->mergeWithCollection($trans, $uid_name)
            ->addSimpleSelectFields($fields)
            ->getFirstObjectFromCollection();
    }

    /**
     * Returns / for default lang, and /<lang> for other
     * @return string
     */
    static public function getLangHomeUrl(){
        return '/'.(LNG == Settings::get('f_default_language') ? '' : LNG);
    }

    /**
     * Return link to map for coordinates
     * @param $coordinates
     * @return string
     */
    static public function mapLink($coordinates){
        return "http://maps.apple.com/maps?q=".$coordinates;
    }

    /**
     * Replaces text surriounded with # with link
     * @param $string
     * @param $href
     * @param array $options
     * @return mixed
     */
    static public function hashToLink($string, $href, $options = [])
    {
        $attrs = [];
        foreach((array)$options as $key=>$val)
        {
            $attrs[] = $key.'="'.$val.'"';
        }
        $link = '<a href="'.$href.'" '.implode(' ', $attrs).'>';
        return preg_replace('~(#)([^#]+)(#)~', ' '.$link.'$2</a> ', $string);

    }

    /**
     * Check ir $url is from the same origin
     * @param $url
     * @return bool
     */
    public static function isUrlFromSameHost($url){
        $url_host = parse_url($url, PHP_URL_HOST);
        return ($url_host == $_SERVER['HTTP_HOST']);
    }
}
