<?php
/**
 * Project: profileoptics
 * File: multilingualEntityRepositoryTrait.php
 * User: markl
 * Date: 05.07.2018
 * Copyright (c) 2018
 */

namespace TMCms\Modules\Simple\Traits;

use TMCms\Orm\Entity;

\defined('INC') or exit;

/**
 * Trait activeEntityRepositoryTrait
 * @package TMCms\Modules\Simple\Traits
 * 
 * @method $this setWhereActive($flag)
 */
trait activeEntityRepositoryTrait
{
    
}