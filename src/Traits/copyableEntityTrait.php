<?php

namespace TMCms\Modules\Simple\Traits;


trait copyableEntityTrait
{
    function copy()
    {
        $this->setId(0);
        $this->save();
        return $this;
    }
}