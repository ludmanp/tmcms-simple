<?php
/**
 * Project: profileoptics
 * File: multilingualEntityRepositoryTrait.php
 * User: markl
 * Date: 05.07.2018
 * Copyright (c) 2018
 */

namespace TMCms\Modules\Simple\Traits;

use TMCms\Orm\Entity;

\defined('INC') or exit;

trait multilingualEntityRepositoryTrait
{
    /**
     * @param $uid
     * @param array $fields
     * @param string $uid_name
     * @return Entity
     */
    static public function getLangEntity($uid, $fields = [], $uid_name = 'uid'){
        $lngs = array_keys(\TMCms\Routing\Languages::getPairs());
        $trans = \TMCms\Admin\Structure\Entity\TranslationRepository::getInstance()->addSimpleSelectFields($lngs);
        $func = 'setWhere'.ucfirst($uid_name);
        $fields = array_merge(['id'], $fields);
        $repo = self::getInstance();
        return $repo
            ->{$func}($uid)
            ->mergeWithCollection($trans, $uid_name)
            ->addSimpleSelectFields($fields)
            ->getFirstObjectFromCollection();
    }
}