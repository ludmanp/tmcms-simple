<?php
/**
 * Project: profileoptics
 * File: multilingualEntityTrait.php
 * User: markl
 * Date: 05.07.2018
 * Copyright (c) 2018
 */

namespace TMCms\Modules\Simple\Traits;

\defined('INC') or exit;

trait multilingualEntityTrait
{
    /**
     * @param string $lng
     * @return mixed
     */
    public function getSlugUrl($lng = LNG): string
    {
        $func = 'get'.ucfirst($lng);

        return $this->{$func}() ?: $this->getId();
    }
}