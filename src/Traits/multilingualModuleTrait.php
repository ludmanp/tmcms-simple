<?php
/**
 * Project: profileoptics
 * File: multilingualModuleTrait.php
 * User: markl
 * Date: 05.07.2018
 * Copyright (c) 2018
 */

namespace TMCms\Modules\Simple\Traits;

use TMCms\Orm\Entity;
use TMCms\Routing\Structure;

\defined('INC') or exit;

trait multilingualModuleTrait
{
    private static $base_url = null;
    protected static $uidName = 'uid';

    /**
     * @return string
     */
    public static function getBaseUrl(){
        if(is_null(self::$base_url)){
            self::$base_url = Structure::getPathByLabel(self::$string_label);
        }
        return self::$base_url;
    }

    /**
     * @param Entity $item
     * @return string
     */
    public static function getUrl($item){
        if(is_scalar($item)){
            return self::getBaseUrl().$item.'/';
        }
        return self::getBaseUrl().$item->getField(self::$uidName).'/';
    }

}